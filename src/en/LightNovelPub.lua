-- {"id":133337,"ver":"1.0.0","libVer":"1.0.0","author":"RaMerry"}

local baseURL = "https://light-novelpub.com"

-- Filter Keys & Values
local ORDER_BY_FILTER = 3
local ORDER_BY_VALUES = { "Popular", "Latest Update" }
local GENRE_FILTER = 4
local GENRE_VALUES = { 
	"Action", "Adult", "Adventure", "Arts", "Chinese", "Comedy", "Drama", "Eastern", "Eastern fantasy", "Ecchi", "Fantasy", "Fantasy romance", "Game", "Gender bender", "Harem", "Historical", "Horror", "Isekai", "Josei", "Lgbt+", "Magic", "Magical realism", "Manhua", "Martial arts", "Mature", "Mecha", "Modern life", "Mystery", "Psychological", "Reincarnation", "Romance", "School life", "Sci-fi", "Seinen", "Shoujo", "Shoujo ai", "Shounen", "Shounen ai", "Slice of life", "Smut", "Sports", "Supernatural", "System", "Tragedy", "Urban", "Urban life", "Video games", "Wuxia", "Xianxia", "Xuanhuan", "Yaoi", "Yuri"
}
local searchFilters = {
	DropdownFilter(ORDER_BY_FILTER, "Order by", ORDER_BY_VALUES),
	DropdownFilter(GENRE_FILTER, "Genre", GENRE_VALUES)
}

local encode = Require("url").encode

local text = function(v)
	return v:text()
end

local function shrinkURL(url)
	return url:gsub("^.-novelpub%.com", "")
end

local function expandURL(url)
	return baseURL .. url
end

local function getPassage(chapterURL)
	local chap = GETDocument(expandURL(chapterURL))
	local title = chap:selectFirst(".chr-text"):text()
	chap = chap:selectFirst("#chr-content")
	chap:child(0):before("<p>" .. title .. "</p>")
	-- Remove empty <p> tags
	local toRemove = {}
	chap:traverse(NodeVisitor(function(v)
		if v:tagName() == "p" and v:text() == "" then
			toRemove[#toRemove+1] = v
		end
		if v:hasAttr("border") then
			v:removeAttr("border")
		end
	end, nil, true))
	for _,v in pairs(toRemove) do
		v:remove()
	end
	return pageOfElem(chap, true)
end

-- Thank god for: https://try.jsoup.org/
local function parseNovel(novelURL, loadChapters) 
	local doc = GETDocument(expandURL(novelURL))
	local content = doc:selectFirst("div.col-xs-12.col-sm-8.col-md-8.desc")
	-- error("Attempting to get chapter list at: " .. novelURL:gsub("/lightnovelpub/", ""), 2)
	local chapterlist = GETDocument("https://light-novelpub.com/ajax/chapter-archive?novelId=" .. novelURL:gsub("/lightnovelpub/", ""))

	local info = NovelInfo {
		title = content:selectFirst("h3"):text(),
		imageURL = doc:select("img"):get(1):attr("src"),
		description = doc:selectFirst(".desc-text"):text(),
		authors = { doc:selectFirst(".info > li:nth-child(2) > a:nth-child(2)"):text() },
		artists = {
			"Translator: " .. doc:selectFirst(".info > li:nth-child(2) > a:nth-child(2)"):text(),
			"Editor: " .. doc:selectFirst(".info > li:nth-child(2) > a:nth-child(2)"):text()
		},
		genres = map(doc:select(".info > li:nth-child(3)"):select("a"), text),
		tags = map(doc:select(".info > li:nth-child(3)"):select("a"), text)
	}

	if loadChapters then
		local chapters = {}
		chapters[#chapters+1] = (mapNotNil(chapterlist:selectFirst(".panel-body"):select("li"), function(v, i)
			return NovelChapter {
				order = i,
				title = v:selectFirst("a"):text(),
				link = shrinkURL(v:selectFirst("a"):attr("href")),
				release = "Unknown"
			}
		end))
		-- local chapterPages = content:selectFirst("a.next.page-numbers")
		-- -- Gets chapters from other pages if they exists!
		-- if chapterPages ~= nil then
		-- 	-- Removing the next button and looping throught the numbered page links
		-- 	content:select("a.next.page-numbers"):remove()
		-- 	map(content:select("a.page-numbers"), function(v)
		-- 		chapters[#chapters+1] = mapNotNil(GETDocument(v:attr("href")):selectFirst("#chapter ul.march1"):select("li"), function(v, i)
		-- 			return NovelChapter {
		-- 				order = i,
		-- 				title = v:selectFirst("a"):text(),
		-- 				link = shrinkURL(v:selectFirst("a"):attr("href")),
		-- 				release = v:selectFirst(".july"):text()
		-- 			}
		-- 			end)
		-- 	end)
		-- end
		info:setChapters(AsList(flatten(chapters)))
	end
	
	return info
end

local function parseListing(listingURL)
	
	local doc = GETDocument(listingURL):selectFirst(".archive > div:nth-child(1)")
	return map(doc:select(".row"), function(v)
		local a = v:selectFirst("a")
		return Novel {
			title = a:attr("title"),
			link = shrinkURL(a:attr("href")),
			imageURL = v:selectFirst("img"):attr("src")
		}
	end)
end

local function getListing(data)
	local page = data[PAGE]
	local orderBy = data[ORDER_BY_FILTER]
	local genre = data[GENRE_FILTER]
	
	local url
	-- Genre filtering only works with popular order by.
	if genre ~= nil and genre ~= 0 then
		url = "/lightnovelpub-genres/" .. genre .. "?page=" .. page
	else
		url = "/sort/popular-lightnovelpub" .. "?page=" .. page
	end

	return parseListing(expandURL(url))
end

local function getSearch(data)
	local docURL = expandURL("/search?keyword=" .. encode(data[QUERY]))
	return parseListing(docURL)
end

return {
	id = 133337,
	name = "Light Novel Pub",
	baseURL = baseURL,
	imageURL = "https://github.com/shosetsuorg/extensions/raw/dev/icons/RainOfSnow.png",
	chapterType = ChapterType.HTML,
	
	listings = { 
		Listing("Popular", true, getListing), 
	},
	getPassage = getPassage,
	parseNovel = parseNovel,
	
	hasSearch = true,
	isSearchIncrementing = false,
	search = getSearch,
	searchFilters = searchFilters,

	shrinkURL = shrinkURL,
	expandURL = expandURL
}
